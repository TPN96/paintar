﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class PaintControler : MonoBehaviour {
	
	public ParticleSystem particleSystemTemplate;

	private bool newPaintVertices;
	private bool paintingOn;
	private Color paintColor;
	private Vector3 previosPosition;

	private List<ParticleSystem> particleSystemList;
	private List<Vector3> currVecticles;
	private ParticleSystem ps;


	void Start(){
		paintingOn = false;
		newPaintVertices = false;
		particleSystemList = new List<ParticleSystem> ();
		ps = Instantiate (particleSystemTemplate);
		currVecticles = new List<Vector3> ();
		paintColor = Color.green;
	}

	void OnEnable(){
		UnityARSessionNativeInterface.ARFrameUpdatedEvent += ARFrameUpdate;
	}
	void OnDestroy(){
		UnityARSessionNativeInterface.ARFrameUpdatedEvent -= ARFrameUpdate;
	}
	public void TogglePaint(){
		paintingOn = !paintingOn;
	}
	public void Reset(){
		foreach (ParticleSystem p in particleSystemList) {
			Destroy (p);
		}
		particleSystemList = new List<ParticleSystem> ();
		Destroy (ps);
		ps = Instantiate (particleSystemTemplate);
		currVecticles = new List<Vector3> ();
	}
	public void Randomize(){
		if (ps.particleCount > 0) {
			SaveParticleSystem ();
		}
		paintColor = Random.ColorHSV ();

	}
	private void SaveParticleSystem(){
		particleSystemList.Add (ps);
		ps = Instantiate (particleSystemTemplate);
		currVecticles = new List<Vector3> ();
	}


	private void ARFrameUpdate(UnityARCamera arCamera){
		Vector3 paintPosition = GetCameraPosition(arCamera) + (Camera.main.transform.position * 0.2f);
		if (Vector3.Distance (paintPosition, previosPosition) > 0.025f) {
			if (paintingOn)
				currVecticles.Add (paintPosition);

			previosPosition = paintPosition;
			newPaintVertices = true;
		}
	}
	void Update(){
		if (paintingOn && newPaintVertices) {
			if (currVecticles.Count > 0) {
				ParticleSystem.Particle[] particles = new ParticleSystem.Particle[currVecticles.Count];
				int index = 0;
				foreach (Vector3 vtx in currVecticles){
					particles[index].position = vtx;
					particles[index].color = paintColor;
					particles[index].size = 0.1f;
					index++;
				}
				ps.SetParticles(particles,currVecticles.Count);
				newPaintVertices = false;
			}
		}
	}

	private Vector3 GetCameraPosition(UnityARCamera cam){
		Matrix4x4 matrix = new Matrix4x4 ();
		matrix.SetColumn (3, cam.worldTransform.column3);
		return UnityARMatrixOps.GetPosition (matrix);
	}

}
